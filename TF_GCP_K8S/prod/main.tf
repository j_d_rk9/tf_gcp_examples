terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.5.0"
    }
  }
  backend "gcs" {
      bucket = ""
      prefix = "terraform/prod"
  }
  required_version = ">= 1.0.11"
}
provider "google" {
  project = ""
}
module "web_app" {
  source = "../modules/web"
  env = "prod"
}
output "ip" {
  value = module.web_app.web_server_ip
}