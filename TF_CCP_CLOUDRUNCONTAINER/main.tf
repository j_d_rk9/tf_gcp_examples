terraform {
  required_version = ">1.0.11"
  required_providers {
    google = {
        source = "hashicorp/google"
        version = "~> 3.53"
    }
  }
}

module "cloud_run" {
  source  = "GoogleCloudPlatform/cloud-run/google"
  version = "~> 0.2.0"
  service_name = "my-app"
  project_id = "using-terraf-263-969c90ac"
  location = "us-central1"
  image = "gcr.io/cloudrun/hello"
  members = ["allUsers"]
}

output "url" {
    value = module.cloud_run.service_url 
}